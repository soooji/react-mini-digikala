/* eslint-disable @typescript-eslint/explicit-module-boundary-types */

import { ProductType } from 'types/product';

// Enum of Types
export enum actionTypes {
  UPDATE = 'UPDATE',
  REMOVE_ITEM = 'REMOVE_ITEM',
  EMPTY_CART = 'EMPTY_CART'
}

// Type of Actions
type UpdateCartAction = {
  type: actionTypes.UPDATE;
  payload: { product: ProductType; remove?: boolean };
};

type RemoveItemAction = {
  type: actionTypes.REMOVE_ITEM;
  payload: { product_id: number };
};

type EmptyCartAction = {
  type: actionTypes.EMPTY_CART;
};

export type CartActionTypes = UpdateCartAction | RemoveItemAction | EmptyCartAction;

// Action Methods
export const updateCartAction = (
  product: ProductType,
  isRemove: boolean
): CartActionTypes => ({
  type: actionTypes.UPDATE,
  payload: {
    product,
    remove: isRemove ?? false
  }
});

export const removeItemAction = (product_id: number): CartActionTypes => ({
  type: actionTypes.REMOVE_ITEM,
  payload: {
    product_id
  }
});

export const emptyCartAction = (): CartActionTypes => ({
  type: actionTypes.EMPTY_CART
});
