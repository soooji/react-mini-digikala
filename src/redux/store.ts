import { createStore } from 'redux';
import { loadState, saveState } from 'redux/localStorage';
import { cartReducer } from './reducers/cart';

// create a makeStore function
const persistedState = loadState();

export const store = createStore(cartReducer, persistedState);

store.subscribe(() => {
  saveState({
    cart: store.getState().cart
  });
});
