import { actionTypes, CartActionTypes } from 'redux/actions/cart';
import { CartItemType } from 'types/cart';

export interface ICart {
  cart: CartItemType[];
}

export const initialState: ICart = {
  cart: []
};

export const cartReducer = (
  state: ICart = initialState,
  action: CartActionTypes
): ICart => {
  switch (action.type) {
    case actionTypes.UPDATE: {
      const newCart: CartItemType[] = [...state.cart];

      const index = state.cart.findIndex(
        (p) => p.product.id === action.payload.product.id
      );

      // Not Found -> Add it
      if (index < 0) {
        newCart.push({
          quantity: 1,
          product: action.payload.product
        });
        return { cart: newCart };
      }

      // Found -> Update Quantity
      const updatedCartItem = Object.assign({}, state.cart[index], {
        quantity: state.cart[index].quantity + (action.payload.remove ? -1 : 1)
      });

      // Was the last one -> Remove it
      if (updatedCartItem.quantity === 0) {
        newCart.splice(index, 1);
      } else {
        newCart.splice(index, 1, updatedCartItem);
      }

      return {
        cart: newCart
      };
    }

    case actionTypes.REMOVE_ITEM: {
      const index = state.cart.findIndex(
        (p) => p.product.id === action.payload.product_id
      );

      if (index < 0) return state;

      const newCart: CartItemType[] = [...state.cart];

      newCart.splice(index, 1);

      return {
        cart: newCart
      };
    }

    case actionTypes.EMPTY_CART: {
      return {
        cart: []
      };
    }

    default:
      return state;
  }
};
