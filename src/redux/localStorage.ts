import { ICart } from 'redux/reducers/cart';

export const loadState = (): ICart | undefined => {
  try {
    const serialized = localStorage.getItem('state');
    if (serialized === null) {
      return undefined;
    }
    return JSON.parse(serialized);
  } catch (error) {
    return undefined;
  }
};

export const saveState = (state: ICart): void => {
  try {
    const serialized = JSON.stringify(state);
    localStorage.setItem('state', serialized);
  } catch (error) {
    console.log(error); // TODO: Handle Error
  }
};
