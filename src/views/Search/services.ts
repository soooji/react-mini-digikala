import axios, { AxiosResponse } from 'axios';
import qs from 'qs';
import { apiConfig, BASE_API } from 'utils';

export type ProductsQueryType = {
  page?: number;
};

export const getProducts = (query: ProductsQueryType): Promise<AxiosResponse> => {
  const url = `${BASE_API}search/?${qs.stringify(query)}`;
  return axios.get(url, apiConfig);
};

export default {
  getProducts
};
