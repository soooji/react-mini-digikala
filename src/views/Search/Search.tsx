import { FC, useState, useEffect } from 'react';
import { ErrorView, Flex, Pagination, Skeleton } from 'components';
import styled from 'styled-components';
import qs from 'qs';
import { Helmet } from 'react-helmet';

import { ProductItem } from './components';
import { ProductType } from 'types/product';
import { getProducts, ProductsQueryType } from './services';
import { useHistory, useLocation } from 'react-router';
import { getLocationQuery, getPageTitle, scrollToTop } from 'utils';
import { ErrorType } from 'types/error';

interface Props {
  className?: string;
}

const Search: FC<Props> = (props) => {
  //  States
  const [products, setProducts] = useState<ProductType[]>([]);
  const [page, setPage] = useState(1);
  const [pagesCount, setPagesCount] = useState(1);

  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [error, setError] = useState<ErrorType>();

  //  Hooks
  const location = useLocation();
  const history = useHistory();

  useEffect(() => {
    scrollToTop();
    getData();
    updateStatesFromURL();
  }, [location]);

  //  Api
  const getData = async () => {
    setError(undefined);
    setIsLoading(true);
    setProducts([]);

    const query: ProductsQueryType = getLocationQuery<ProductsQueryType>(
      location?.search ?? ''
    );

    getProducts(query)
      .then((r) => {
        const data = r?.data;

        if (data?.status !== 200)
          return setError({ title: data?.message ?? 'خطا در دریافت لیست محصولات' });

        setProducts(data?.data?.products ?? []);
        setPagesCount(data?.data?.pager?.total_pages ?? 1);
      })
      .catch((e) => {
        return setError({
          title: e?.response?.data?.message ?? 'خطا در دریافت لیست محصولات'
        });
      })
      .finally(() => setIsLoading(false));
  };

  //  Methods
  const goPage = (pageIndex: number) => {
    if (pageIndex <= 0) return;
    const query = getLocationQuery<ProductsQueryType>(location?.search ?? '');
    query['page'] = pageIndex;
    updatePageQuery(query);
  };

  const updatePageQuery = (query: Record<string, unknown>): void => {
    history.push({
      search: qs.stringify(query)
    });
  };

  const updateStatesFromURL = () => {
    const query: ProductsQueryType = getLocationQuery<ProductsQueryType>(
      location?.search ?? ''
    );
    setPage(parseInt(`${query?.page ?? 1}`));
  };

  return (
    <Flex className={props?.className}>
      <Helmet>
        <title>{getPageTitle('جستجوی محصول')}</title>
      </Helmet>

      <div className="filters"></div>
      <div className="content-side">
        {!error ? (
          <Flex
            className="products-list"
            fw="wrap"
            jc="flex-start"
            ai="stretch"
            ac="stretch">
            {isLoading
              ? [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].map((_, k) => (
                  <Skeleton key={k} height="426px" />
                ))
              : products.map((v) => (
                  <div key={v.id}>
                    <ProductItem
                      images={v.images}
                      id={v.id}
                      title={v.title}
                      price={v.price}
                      rating={v.rating}
                      status={v.status}
                    />
                  </div>
                ))}
          </Flex>
        ) : (
          <ErrorView title={error.title} />
        )}

        <Flex fullWidth ai="center" jc="center">
          <Pagination
            pageCount={pagesCount}
            activePage={page}
            onPageChange={goPage}
          />
        </Flex>
      </div>
    </Flex>
  );
};

const StyledSearch = styled(Search)`
  margin-bottom: ${({ theme }) => theme.space(3)};

  @media only screen and (max-width: ${({ theme }) => theme.responsive.xl}) {
    flex-direction: column;
  }

  .content-side {
    width: 100%;
    > .products-list {
      width: 100%;
      margin-top: ${({ theme }) => theme.space(2)};
      margin-bottom: ${({ theme }) => theme.space(2)};
      > div {
        width: calc(100% / 4 - ${({ theme }) => theme.space(2)});
        margin-bottom: ${({ theme }) => theme.space(2)};
        margin-left: ${({ theme }) => theme.space(1)};
        margin-right: ${({ theme }) => theme.space(1)};

        @media only screen and (max-width: 1450px) {
          width: calc(100% / 3 - ${({ theme }) => theme.space(2)});
        }
        @media only screen and (max-width: ${({ theme }) => theme.responsive.lg}) {
          width: calc(100% / 2 - ${({ theme }) => theme.space(2)});
        }
        @media only screen and (max-width: ${({ theme }) => theme.responsive.md}) {
          width: 100%;
        }
      }
    }
  }
  > .filters {
    width: 270px;
    min-height: calc(100vh - 100px);
    background-color: white;
    flex-shrink: 0;
    border-radius: ${({ theme }) => theme.borderRadius.normal};
    box-shadow: ${({ theme }) => theme.shadows[0]};
    margin-top: ${({ theme }) => theme.space(2)};
    ${({ theme }) => theme.marginInlineEnd(2)};

    @media only screen and (max-width: ${({ theme }) => theme.responsive.xl}) {
      min-height: 60px;
      width: 100%;
    }
  }
`;

export default StyledSearch;
