import { FC } from 'react';
import { faStar, faStore } from '@fortawesome/free-solid-svg-icons';
import clsx from 'clsx';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import { ICart } from 'redux/reducers/cart';
import { Flex, Icon, AddToCart } from 'components';
import {
  addCommas,
  computeDiscountPercent,
  CURRENCY,
  getProductQuantity,
  priceConvertor
} from 'utils';
import { Link } from 'react-router-dom';
import { ProductType } from 'types/product';

type Props = {
  className?: string;
} & ProductType;

const ProductItem: FC<Props> = (props) => {
  const cart = useSelector<ICart, ICart['cart']>((state) => state.cart);
  const quantity = getProductQuantity(cart, props.id);

  const discountPercent = computeDiscountPercent(
    props.price.rrp_price,
    props.price.selling_price
  );
  const link = `/product/${props.id}/${encodeURIComponent(props.title)}`;

  return (
    <div className={clsx(props?.className, 'noselect')}>
      <Link to={link}>
        <a>
          <div className={clsx('special', discountPercent === 0 && '--hidden')}>
            فروش ویژه
          </div>

          <Flex jc="center" ai="center" className="image-container">
            <img src={props.images.main} alt={props.title} />
          </Flex>
        </a>
      </Link>

      <Link to={link}>
        <a>
          <div className="product-title">{props.title}</div>
        </a>
      </Link>

      <div className="rating">
        <Icon icon={faStar} className="rating__icon" />
        <span className="rating__text">
          {props.rating?.rate ?? 0} <span>({props.rating?.count ?? 0})</span>
        </span>
      </div>

      <Flex className="prices" jc="flex-end">
        <Flex
          className={clsx('prices__discount', discountPercent === 0 && '--hidden')}>
          <div>{addCommas(priceConvertor(props.price.rrp_price ?? 0))}</div>
          <span>{discountPercent} %</span>
        </Flex>
      </Flex>

      <Flex className="footer" jc="space-between" fw="wrap">
        <div className="footer__add2cart">
          <AddToCart quantity={quantity} product={props} />
        </div>
        <Flex className="footer__price" jc="flex-end" ai="flex-start">
          <div>{addCommas(priceConvertor(props.price.selling_price ?? 0))}</div>
          <span>{CURRENCY}</span>
        </Flex>
      </Flex>

      <Flex className="seller" jc="flex-start" ai="center">
        <Icon icon={faStore} className="seller__icon" />
        <div className="seller__name">دیجی کالا</div>
      </Flex>
    </div>
  );
};

const StyledProductItem = styled(ProductItem)`
  background: white;
  width: 100%;
  height: 100%;
  overflow: hidden;
  transition: ease box-shadow 0.2s, ease transform 0.2s;
  transform: scale(1);
  box-shadow: ${({ theme }) => theme.shadows[0]};
  border-radius: ${({ theme }) => theme.borderRadius.normal};
  padding: ${({ theme }) => `${theme.space(1)} ${theme.space(2)}`};

  .image-container {
    padding-top: ${({ theme }) => theme.space(2)};
    padding-bottom: ${({ theme }) => theme.space(2)};
    > img {
      width: 190px;
      height: 190px;
      object-fit: contain;
      object-position: center;
      margin-left: auto;
      margin-right: auto;
    }
  }

  .special {
    font-size: 0.8rem;
    font-weight: 400;
    text-align: center;
    color: ${({ theme }) => theme.colors.primary.dark};
    padding-bottom: ${({ theme }) => theme.space(1)};
    border-bottom: 1px solid ${({ theme }) => theme.colors.primary.dark};
    &.--hidden {
      visibility: hidden;
      opacity: 0;
    }
  }
  .product-title {
    font-size: 0.8rem;
    height: 3rem;
    line-height: 1.5rem;
    font-weight: 400;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    color: ${({ theme }) => theme.colors.grey[3]};
    padding-bottom: ${({ theme }) => theme.space(1)};
  }

  .rating {
    direction: ltr;
    text-align: left;
    &__icon {
      color: #fac74b;
      font-size: 0.8rem;
    }
    &__text {
      font-size: 0.9rem;
      color: ${({ theme }) => theme.colors.grey[4]};
      ${({ theme }) => theme.marginInlineStart(0.5)};
      > span {
        color: ${({ theme }) => theme.colors.grey[5]};
        font-size: 0.65rem;
      }
    }
  }

  .prices {
    margin-top: ${({ theme }) => theme.space(1)};
    &__discount {
      &.--hidden {
        visibility: hidden;
        opacity: 0;
      }
      > span {
        font-weight: 600;
        font-size: 0.9rem;
        padding: ${({ theme }) => `${theme.space(0.4)} ${theme.space(0.9)}`};
        height: 25px;
        border-radius: 20px;
        background-color: ${({ theme }) => theme.colors.primary.main};
        color: white;
      }
      > div {
        font-weight: 400;
        font-size: 0.95rem;
        text-decoration: line-through;
        color: ${({ theme }) => theme.colors.grey[5]};
        padding-top: ${({ theme }) => theme.space(0.5)};
        ${({ theme }) => theme.paddingInlineEnd(1)};
      }
    }
  }

  .footer {
    &__add2cart {
      margin-top: ${({ theme }) => theme.space(-2.2)};
    }
    &__price {
      margin-top: ${({ theme }) => theme.space(0.5)};
      > span {
        font-weight: 400;
        font-size: 0.8rem;
        color: ${({ theme }) => theme.colors.grey[3]};
        padding-top: ${({ theme }) => theme.space(0.5)};
      }
      > div {
        font-weight: 500;
        font-size: 1.2rem;
        color: ${({ theme }) => theme.colors.grey[1]};
        ${({ theme }) => theme.paddingInlineEnd(0.5)};
      }
    }
  }

  .seller {
    opacity: 0;
    transition: ease opacity 0.2s;
    border-top: solid 0.5px ${({ theme }) => theme.colors.grey[8]};
    margin-top: ${({ theme }) => theme.space(2)};
    padding-top: ${({ theme }) => theme.space(0.8)};
    &__icon {
      font-size: 0.8rem;
      color: ${({ theme }) => theme.colors.grey[5]};
    }
    &__name {
      font-size: 0.9rem;
      font-weight: 500;
      color: ${({ theme }) => theme.colors.grey[2]};
      ${({ theme }) => theme.paddingInlineStart(1)};
    }
  }

  &:hover {
    box-shadow: ${({ theme }) => theme.shadows[1]};
    transform: scale(1.03);
    .seller {
      opacity: 1;
    }
  }
`;

export default StyledProductItem;
