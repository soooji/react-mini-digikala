import { FC } from 'react';
import styled from 'styled-components';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { Flex, Icon } from 'components';
import clsx from 'clsx';

interface Props {
  className?: string;
  text: string;
  icon?: IconProp;
  coloredIcon?: boolean;
}

const DetailRow: FC<Props> = (props) => {
  return (
    <Flex ai="flex-start" className={props?.className}>
      {props.icon ? (
        <Icon
          icon={props.icon}
          className={clsx('details__row__icon', props.coloredIcon && '--colored')}
        />
      ) : null}
      <div className="details__row__text">{props.text}</div>
    </Flex>
  );
};

const StyledDetailRow = styled(DetailRow)`
  margin-top: ${({ theme }) => theme.space(1)};
  .row-icon {
    font-size: 0.8rem;
    color: ${({ theme }) => theme.colors.grey[5]};
    min-width: 15px;
    text-align: center;
    &.--colored {
      color: ${({ theme }) => theme.colors.success.main};
    }
  }
  .row-text {
    font-weight: 400;
    font-size: 0.75rem;
    color: ${({ theme }) => theme.colors.grey[4]};
    ${({ theme }) => theme.marginInlineStart(1)};
  }
`;

export default StyledDetailRow;
