import { FC } from 'react';
import styled from 'styled-components';
import { useDispatch } from 'react-redux';
import { Flex, Icon } from 'components';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { removeItemAction } from 'redux/actions/cart';

interface Props {
  className?: string;
  productId: number;
}

const DeleteCartItem: FC<Props> = (props) => {
  const dispatch = useDispatch();

  const onRemoveCartItem = () => {
    dispatch(removeItemAction(props.productId));
  };

  return (
    <Flex
      className={props?.className}
      ai="center"
      jc="center"
      onClick={onRemoveCartItem}>
      <Icon icon={faTrash} />
      <div>حذف</div>
    </Flex>
  );
};

const StyledDeleteCartItem = styled(DeleteCartItem)`
  min-height: 44px;
  color: ${({ theme }) => theme.colors.grey[4]};
  padding: 0 ${({ theme }) => theme.space(2)};
  cursor: pointer;
  span {
    font-size: 0.8rem;
  }
  div {
    font-size: 0.8rem;
    font-weight: 350;
    ${({ theme }) => theme.marginInlineStart(1)};
  }
`;

export default StyledDeleteCartItem;
