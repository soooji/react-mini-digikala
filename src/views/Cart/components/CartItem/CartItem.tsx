import { FC } from 'react';
import styled from 'styled-components';
import clsx from 'clsx';
import { faSave, faShieldAlt, faStore } from '@fortawesome/free-solid-svg-icons';

import { AddToCart, Flex } from 'components';
import { DeleteCartItem, DetailRow } from './components';
import { addCommas, CURRENCY, priceConvertor } from 'utils';
import { CartItemType } from 'types/cart';

type Props = {
  className?: string;
} & CartItemType;

const CartItem: FC<Props> = ({ className, quantity, product }) => {
  return (
    <Flex ai="flex-start" className={clsx(className, 'common-card')}>
      <div className="image">
        <img src={product.images.main} alt={product.title} />
      </div>

      <div className="details">
        <h3>{product.title}</h3>

        <DetailRow text="گارانتی اصالت و سلامت فیزیکی کالا" icon={faShieldAlt} />
        <DetailRow text="مدرن اس سی ام" icon={faStore} />
        <DetailRow text="موجود در انبار دیجی کالا" icon={faSave} coloredIcon />

        <Flex ai="flex-end" jc="space-between" fullWidth fw="wrap">
          <Flex ai="flex-end" fw="wrap" className="actions-side">
            <AddToCart
              quantity={quantity}
              product={product}
              className="counter"
              secondary
            />
            <DeleteCartItem productId={product.id} className="delete-item" />
          </Flex>
          <Flex className="total-price">
            <div>
              {addCommas(priceConvertor(product.price.selling_price * quantity))}
            </div>
            <span>{CURRENCY}</span>
          </Flex>
        </Flex>
      </div>
    </Flex>
  );
};

const StyledCartItem = styled(CartItem)`
  padding: ${({ theme }) => `${theme.space(3)} ${theme.space(1)}`};
  margin-bottom: ${({ theme }) => theme.space(1)};

  @media only screen and (max-width: ${({ theme }) => theme.responsive.md}) {
    flex-direction: column;
  }

  > .image {
    margin: ${({ theme }) => theme.space(2)};
    width: 140px;
    height: auto;
    max-width: 100%;

    @media only screen and (max-width: ${({ theme }) => theme.responsive.md}) {
      margin: 0 auto ${({ theme }) => theme.space(2)} auto;
    }

    img {
      width: 100%;
      height: auto;
    }
  }

  .details {
    width: 100%;
    ${({ theme }) => theme.marginInlineStart(2)};
    h3 {
      font-size: 0.95rem;
      color: ${({ theme }) => theme.colors.grey[2]};
      font-weight: 400;
      margin-top: 0;
      margin-bottom: ${({ theme }) => theme.space(3)};
    }
    &__row {
      margin-top: ${({ theme }) => theme.space(1)};
      &__icon {
        font-size: 0.8rem;
        color: ${({ theme }) => theme.colors.grey[5]};
        min-width: 15px;
        text-align: center;
        &.--colored {
          color: ${({ theme }) => theme.colors.success.main};
        }
      }
      &__text {
        font-weight: 400;
        font-size: 0.75rem;
        color: ${({ theme }) => theme.colors.grey[4]};
        ${({ theme }) => theme.marginInlineStart(1)};
      }
    }
  }

  .actions-side {
    .counter {
      margin-top: ${({ theme }) => theme.space(2)};
    }
    @media only screen and (max-width: ${({ theme }) => theme.responsive.md}) {
      margin-bottom: ${({ theme }) => theme.space(2)};
    }
  }

  .delete-item {
    ${({ theme }) => theme.marginInlineStart(1)};
  }

  .total-price {
    ${({ theme }) => theme.paddingInlineEnd(2)};
    ${({ theme }) => theme.marginInlineStart('auto')};
    > div {
      font-size: 1.1rem;
      font-weight: 400;
      color: ${({ theme }) => theme.colors.grey[1]};
    }
    > span {
      font-size: 0.8rem;
      font-weight: 300;

      color: ${({ theme }) => theme.colors.grey[2]};
      margin-top: ${({ theme }) => theme.space(0.5)};
      ${({ theme }) => theme.marginInlineStart(0.7)};
    }
  }
`;

export default StyledCartItem;
