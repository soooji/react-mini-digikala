import clsx from 'clsx';
import { Flex } from 'components';
import { FC } from 'react';
import styled from 'styled-components';
import { addCommas, CURRENCY, priceConvertor } from 'utils';

interface Props {
  className?: string;
  price: number;
  title: string;
  bold?: boolean;
  coloredPrice?: boolean;
}

const PriceRow: FC<Props> = ({
  className,
  price,
  title,
  bold = false,
  coloredPrice = false
}) => {
  return (
    <Flex jc="space-between" className={clsx(className, bold && '--bold')}>
      <div className="txt">{title}</div>
      <Flex className={clsx('price', coloredPrice && '--colored')}>
        <div>{addCommas(priceConvertor(price))}</div>
        <span>{CURRENCY}</span>
      </Flex>
    </Flex>
  );
};

const StyledPriceRow = styled(PriceRow)`
  margin-bottom: ${({ theme }) => theme.space(1)};
  .txt {
    font-size: 0.8rem;
    font-weight: 350;
    color: ${({ theme }) => theme.colors.grey[5]};
  }
  .price {
    > div {
      font-size: 1rem;
      font-weight: 400;
      color: ${({ theme }) => theme.colors.grey[4]};
    }
    > span {
      font-size: 0.7rem;
      font-weight: 300;

      color: ${({ theme }) => theme.colors.grey[3]};
      margin-top: ${({ theme }) => theme.space(0.5)};
      ${({ theme }) => theme.marginInlineStart(0.5)};
    }
    &.--colored {
      * {
        color: ${({ theme }) => theme.colors.primary.main};
      }
    }
  }
  &.--bold {
    * {
      color: ${({ theme }) => theme.colors.grey[1]} !important;
      font-weight: bold;
    }
  }
`;

export default StyledPriceRow;
