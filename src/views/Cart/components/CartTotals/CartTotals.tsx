import { FC } from 'react';
import clsx from 'clsx';
import styled from 'styled-components';
import { PriceRow } from './components';
import { Button } from 'components';
import { TotalPricesType } from 'types/cart';

interface Props {
  className?: string;
  totals: TotalPricesType;
}

const CartTotals: FC<Props> = ({ className, totals }) => {
  return (
    <div className={clsx(className, 'common-card')}>
      <PriceRow title="قیمت کالاها" price={totals.realPrice} />
      <PriceRow title="تخفیف کالاها" price={totals.discount.price} coloredPrice />
      <hr />
      <PriceRow title="جمع سبد خرید" price={totals.payable} bold />
      <Button className="checkout-button" bold>
        ادامه فرایند خرید
      </Button>
    </div>
  );
};

const StyledCartTotals = styled(CartTotals)`
  padding: ${({ theme }) => theme.space(2)};
  hr {
    border: none;
    height: 1px;
    width: 100%;
    margin-top: ${({ theme }) => theme.space(1.5)};
    margin-bottom: ${({ theme }) => theme.space(1.5)};
    background-color: ${({ theme }) => theme.colors.grey[8]};
  }
  .checkout-button {
    width: 100%;
    min-height: 50px;
    margin: ${({ theme }) => theme.space(1)} 0 0 0;
    border-radius: ${({ theme }) => theme.borderRadius.normal};
  }
`;

export default StyledCartTotals;
