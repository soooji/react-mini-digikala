import { FC } from 'react';
import styled from 'styled-components';
import { useDispatch } from 'react-redux';

import { Button, EmptyCart, Flex } from 'components';
import { CartItem, CartTotals } from './components';
import { ICart } from 'redux/reducers/cart';
import { useSelector } from 'react-redux';
import { computeCartTotals, getPageTitle } from 'utils';
import { emptyCartAction } from 'redux/actions/cart';
import { useHistory } from 'react-router';
import { Helmet } from 'react-helmet';

interface Props {
  className?: string;
}

const Cart: FC<Props> = (props) => {
  const cart = useSelector<ICart, ICart['cart']>((state) => state.cart);
  const history = useHistory();

  const totals = computeCartTotals(cart);

  const dispatch = useDispatch();

  const onEmptyCart = () => {
    dispatch(emptyCartAction());
    history.push('/');
  };

  return (
    <div className={props?.className}>
      <Helmet>
        <title>{getPageTitle('سبد خرید من')}</title>
      </Helmet>

      <Flex className="cart-container">
        <div className="cart-items">
          {cart.length === 0 ? (
            <div className="empty-container common-card">
              <EmptyCart />
            </div>
          ) : (
            cart.map((v) => (
              <CartItem
                key={v.product.id}
                product={v.product}
                quantity={v.quantity}
              />
            ))
          )}
        </div>

        <div className="summary">
          <CartTotals totals={totals} />
          <Button className="clear-cart" mode="grey" onClick={onEmptyCart}>
            پاک کردن سبد خرید
          </Button>
        </div>
      </Flex>
    </div>
  );
};

const StyledCart = styled(Cart)`
  margin-top: ${({ theme }) => theme.space(10)};
  .empty-container {
    width: 100%;
    padding: ${({ theme }) => theme.space(10)};
  }
  .clear-cart {
    width: 100%;
    min-height: 45px;
    font-size: 0.7rem;
    margin: ${({ theme }) => theme.space(1)} 0 0 0;
    border-radius: ${({ theme }) => theme.borderRadius.normal};
  }
  .cart-container {
    margin-left: auto;
    margin-right: auto;
    width: 100%;
    max-width: 1300px;
    flex-shrink: 0;

    @media only screen and (max-width: ${({ theme }) => theme.responsive.md}) {
      flex-direction: column;
    }
  }
  .cart-items {
    width: 100%;
    ${({ theme }) => theme.marginInlineEnd(2)};
  }
  .summary {
    width: 400px;

    @media only screen and (max-width: ${({ theme }) => theme.responsive.md}) {
      width: 100%;
    }
  }
`;

export default StyledCart;
