import { FC, useEffect, useState } from 'react';
import { useParams } from 'react-router';
import styled from 'styled-components';
import { Helmet } from 'react-helmet';

import { BreadCrumb, Flex, AddToCart, Skeleton } from 'components';
import { RatingStatus, Prices } from './components';
import { ProductType } from 'types/product';
import { getProduct } from './services';
import { ICart } from 'redux/reducers/cart';
import { useSelector } from 'react-redux';
import { getPageTitle, getProductQuantity } from 'utils';

interface Props {
  className?: string;
}

const Product: FC<Props> = (props) => {
  const { slug } = useParams<{ slug: string | undefined }>();
  const productId = slug ? parseInt(slug) : 0;

  const [isLoading, setIsLoading] = useState(false);
  const [product, setProduct] = useState<ProductType>();

  const cart = useSelector<ICart, ICart['cart']>((state) => state.cart);
  const quantity = product?.id ? getProductQuantity(cart, product.id) : 0;

  useEffect(() => {
    if (productId !== 0) {
      getData(productId);
    }
  }, [productId]);

  //  Api
  const getData = async (pId: number) => {
    setIsLoading(true);

    getProduct(pId)
      .then((r) => {
        const data = r?.data;
        if (data?.status !== 200) {
          throw new Error(data?.message ?? 'خطا در دریافت اطلاعات!');
        }

        setProduct(data?.data?.product ?? []);
      })
      .catch((e) => console.log(`e`, e))
      .finally(() => setIsLoading(false));
  };

  const breadCrumbs = [
    { title: 'دیجی‌کالا', link: '#?1' },
    { title: 'کالای دیجیتال', link: '#?2' },
    { title: 'لوازم جانبی کالای دیجیتال', link: '#?3' },
    { title: 'کابل و مبدل', link: '#?4' }
  ];

  const brandChain = [
    { title: 'لیتو', link: '#?1' },
    { title: 'کابل و مبدل لیتو', link: '#?1' }
  ];

  return (
    <div className={props?.className}>
      <Helmet>
        <title>{getPageTitle(product?.title ?? 'اطلاعات محصول')}</title>
      </Helmet>

      <BreadCrumb items={breadCrumbs} className="bread-crumbs" />
      <article className="details-container">
        {isLoading ? (
          <Flex>
            <section className="image">
              <Skeleton width="100%" height="100%" />
            </section>
            <section className="details">
              <Skeleton width="200px" height="20px" />
              <h1>
                <Skeleton width="300px" height="30px" />
              </h1>

              <Skeleton width="250px" height="25px" />
              <br />
              <Skeleton width="250px" height="25px" />

              <br />
              <Skeleton width="200px" height="50px" />
            </section>
          </Flex>
        ) : product ? (
          <Flex>
            <section className="image">
              <img src={product?.images?.main} alt={product.title} />
            </section>
            <section className="details">
              <BreadCrumb items={brandChain} />
              <h1>{product.title ?? '-'}</h1>

              <RatingStatus
                offers={{ count: 13, rate: 100 }}
                rating={product.rating}
              />

              <Prices
                price={product.price.rrp_price}
                sellingPrice={product.price.selling_price}
                className="price"
              />

              <AddToCart
                quantity={quantity}
                product={product}
                className="add2cart"
                type="large"
              />
            </section>
          </Flex>
        ) : null}
      </article>
    </div>
  );
};

const StyledProduct = styled(Product)`
  margin-top: ${({ theme }) => theme.space(1)};
  > .bread-crumbs {
    padding: ${({ theme }) => `${theme.space(1)} ${theme.space(2)}`};
  }
  > .details-container {
    background: white;
    box-shadow: ${({ theme }) => theme.shadows[0]};
    width: 100%;
    padding: ${({ theme }) => theme.space(2)};
    border-radius: ${({ theme }) => theme.borderRadius.outside};
    min-height: 200px;
    margin-top: ${({ theme }) => theme.space(1)};
    > div {
      @media only screen and (max-width: ${({ theme }) => theme.responsive.md}) {
        flex-direction: column;
      }
    }
    .image {
      flex-shrink: 0;
      width: 320px;
      height: 320px;
      max-width: 100%;
      margin: ${({ theme }) => theme.space(5)};

      @media only screen and (max-width: ${({ theme }) => theme.responsive.md}) {
        height: auto;
        margin: ${({ theme }) => theme.space(2)} 0 ${({ theme }) => theme.space(2)} 0;
      }

      > img {
        width: 100%;
        height: 100%;
        object-fit: contain;
        object-position: center;
      }
    }
    .details {
      padding-top: ${({ theme }) => theme.space(3)};
      h1 {
        font-weight: 400;
        font-size: 1.1rem;
        margin-top: ${({ theme }) => theme.space(1)};
        color: ${({ theme }) => theme.colors.grey[2]};
        margin-bottom: ${({ theme }) => theme.space(4)};
      }
      > .add2cart {
        margin-top: ${({ theme }) => theme.space(2)};
      }
      > .price {
        margin-top: ${({ theme }) => theme.space(4)};
      }
    }
  }
`;

export default StyledProduct;
