import axios, { AxiosResponse } from 'axios';
import { apiConfig, BASE_API } from 'utils';

export const getProduct = (productId: number): Promise<AxiosResponse> => {
  const url = `${BASE_API}product/${productId}/`;
  return axios.get(url, apiConfig);
};

export default {
  getProduct
};
