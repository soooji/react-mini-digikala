import { FC } from 'react';
import { Icon } from 'components';
import styled from 'styled-components';
import { faStar, faThumbsUp } from '@fortawesome/free-solid-svg-icons';

interface Props {
  className?: string;
  offers: { count: number; rate: number };
  rating: { count: number; rate: number };
}

const Product: FC<Props> = (props) => {
  return (
    <div className={props?.className}>
      <div className="offers">
        <Icon icon={faThumbsUp} className="offers__icon" />
        <span className="offers__text">
          {props.offers.rate ?? 0}٪ ({props.offers.count ?? 0} نفر) از خریداران، این
          کالا را پیشنهاد کرده‌اند{' '}
        </span>
      </div>
      <div className="rating">
        <Icon icon={faStar} className="rating__icon" />
        <span className="rating__text">
          {props.rating?.rate ?? 0} <span>({props.rating?.count ?? 0})</span>
        </span>
      </div>
    </div>
  );
};

const StyledProduct = styled(Product)`
  .offers {
    &__icon {
      color: ${({ theme }) => theme.colors.success.main};
      font-size: 0.9rem;
    }
    &__text {
      font-size: 0.75rem;
      color: ${({ theme }) => theme.colors.grey[4]};
      ${({ theme }) => theme.marginInlineStart(1.5)};
      > span {
        color: ${({ theme }) => theme.colors.grey[5]};
        font-size: 0.65rem;
      }
    }
  }
  .rating {
    margin-top: ${({ theme }) => theme.space(1)};
    &__icon {
      color: #fac74b;
      font-size: 0.9rem;
    }
    &__text {
      font-size: 0.75rem;
      color: ${({ theme }) => theme.colors.grey[4]};
      ${({ theme }) => theme.marginInlineStart(1.5)};
      > span {
        color: ${({ theme }) => theme.colors.grey[5]};
        font-size: 0.65rem;
      }
    }
  }
`;

export default StyledProduct;
