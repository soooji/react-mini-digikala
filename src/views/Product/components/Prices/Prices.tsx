import { FC } from 'react';
import styled from 'styled-components';
import clsx from 'clsx';

import { Flex } from 'components';
import { addCommas, computeDiscountPercent, CURRENCY, priceConvertor } from 'utils';

interface Props {
  className?: string;
  price: number;
  sellingPrice: number;
}

const Prices: FC<Props> = (props) => {
  const discountPercent = computeDiscountPercent(props.price, props.sellingPrice);

  return (
    <div className={props?.className}>
      <div className="title">قیمت فروشنده:</div>
      <Flex className={clsx('discount', discountPercent === 0 && '--hidden')}>
        <div>{addCommas(priceConvertor(props.price ?? 0))}</div>
        <span>{discountPercent} %</span>
      </Flex>

      <Flex className="price" jc="flex-start" ai="flex-start">
        <div>{addCommas(priceConvertor(props.sellingPrice ?? 0))}</div>
        <span>{CURRENCY}</span>
      </Flex>
    </div>
  );
};

const StyledPrices = styled(Prices)`
  .title {
    font-size: 0.8rem;
    font-weight: 400;
    color: ${({ theme }) => theme.colors.grey[5]};
    margin-bottom: ${({ theme }) => theme.space(2)};
  }
  .discount {
    &.--hidden {
      visibility: hidden;
      opacity: 0;
      display: none;
    }
    > span {
      font-weight: 600;
      font-size: 0.9rem;
      padding: ${({ theme }) => `${theme.space(0.4)} ${theme.space(1)}`};
      height: 1.6rem;
      border-radius: 20px;
      background-color: ${({ theme }) => theme.colors.primary.main};
      color: white;
    }
    > div {
      font-weight: 400;
      font-size: 1.2rem;
      text-decoration: line-through;
      color: ${({ theme }) => theme.colors.grey[5]};
      ${({ theme }) => theme.paddingInlineEnd(1)};
    }
  }
  .price {
    margin-top: ${({ theme }) => theme.space(0.3)};
    > span {
      font-weight: 400;
      font-size: 0.9rem;
      color: ${({ theme }) => theme.colors.grey[3]};
      padding-top: ${({ theme }) => theme.space(0.7)};
    }
    > div {
      font-weight: 500;
      font-size: 1.5rem;
      color: ${({ theme }) => theme.colors.grey[1]};
      ${({ theme }) => theme.paddingInlineEnd(0.5)};
    }
  }
`;

export default StyledPrices;
