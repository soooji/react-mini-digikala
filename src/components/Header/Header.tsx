import { FC, ReactNode, useState } from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { useWindowScrollPosition } from 'rooks';
import { ICart } from 'redux/reducers/cart';
import { Flex } from 'components';
import { QuickCart, CartDetail } from './components';
import DigikalaLogo from 'assets/images/digikala-logo__color.svg';
import { getTotalQuantity } from 'utils';
import clsx from 'clsx';

interface Props {
  children?: ReactNode;
  className?: string;
}

const HeaderCMP: FC<Props> = ({ className }) => {
  const [forceKey, setForceKey] = useState(1);
  const { scrollY } = useWindowScrollPosition();

  const cart = useSelector<ICart, ICart['cart']>((state) => state.cart);
  const totalQuantity = getTotalQuantity(cart); //  Sum of Cart Item Quantities

  return (
    <Flex
      className={clsx(className, scrollY > 70 && '--float')}
      jc="space-between"
      ai="center">
      <Flex jc="flex-start" ai="center">
        <Link to="/" className="main-logo">
          <a>
            <span>Digikala</span>
          </a>
        </Link>
      </Flex>

      <Flex jc="flex-end" ai="center">
        <QuickCart
          key={forceKey}
          cartDetail={
            <CartDetail cart={cart} onClose={() => setForceKey(forceKey + 1)} />
          }
          count={totalQuantity}
        />
      </Flex>
    </Flex>
  );
};

const Header = styled(HeaderCMP)`
  position: fixed;
  right: ${({ theme }) => theme.space(1)};
  left: ${({ theme }) => theme.space(1)};
  top: ${({ theme }) => theme.space(1)};
  z-index: 1111;
  background: white;
  box-shadow: ${({ theme }) => theme.shadows[0]};
  min-height: 65px;
  padding: ${({ theme }) => theme.space(2)};
  border-radius: ${({ theme }) => theme.borderRadius.outside};
  transition: ease box-shadow 0.4s, ease top 0.4s, ease border-radius 0.4s;

  &.--float {
    box-shadow: ${({ theme }) => theme.shadows[5]};
    border-top-right-radius: 0;
    border-top-left-radius: 0;
    top: 0;
  }

  > .__aside {
    ${({ theme }) => theme.paddingInlineStart('2px')};
    ${({ theme }) => theme.paddingInlineEnd('2px')};
    width: calc(50% - 75px);
  }

  a.main-logo {
    background-image: url(${DigikalaLogo});
    background-size: contain;
    width: 115px;
    height: 30px;
    span {
      visibility: hidden;
    }
  }
`;

export default Header;
