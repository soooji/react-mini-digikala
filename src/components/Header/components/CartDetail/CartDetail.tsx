import { FC } from 'react';
import { Button, EmptyCart, Flex } from 'components';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { CartItemType } from 'types/cart';
import { addCommas, computeCartTotals, CURRENCY, priceConvertor } from 'utils';
import { MiniCartItem } from './components';

interface Props {
  className?: string;
  cart: CartItemType[];
  onClose: () => void;
}

const CartDetail: FC<Props> = ({ className, cart, onClose }) => {
  const totals = computeCartTotals(cart);

  return (
    <div className={className}>
      <div className="items">
        {cart.length === 0 ? (
          <EmptyCart />
        ) : (
          cart.map((v) => (
            <MiniCartItem
              key={v.product.id}
              quantity={v.quantity}
              product={v.product}
              onClick={onClose}
            />
          ))
        )}
      </div>
      {totals.payable > 0 ? (
        <Flex className="footer" jc="space-between">
          <div className="total">
            <div className="total__title">قابل پرداخت</div>
            <Flex className="total__price">
              <div>{addCommas(priceConvertor(totals.payable))}</div>
              <span>{CURRENCY}</span>
            </Flex>
          </div>

          <Link to="/cart" onClick={onClose}>
            <Button className="checkout">ثبت سفارش</Button>
          </Link>
        </Flex>
      ) : null}
    </div>
  );
};

const StyledCartDetail = styled(CartDetail)`
  width: 280px;
  max-width: calc(100vw - ${({ theme }) => theme.space(6)});

  .items {
    height: 250px;
    overflow-y: auto;
    box-sizing: border-box;
    ::-webkit-scrollbar {
      width: 4px;
      border-radius: 20px;
    }
    /* Track */
    ::-webkit-scrollbar-track {
      border-radius: 20px;
      background: rgba(0, 0, 0, 0.05);
    }
    /* Handle */
    ::-webkit-scrollbar-thumb {
      border-radius: 20px;
      background: rgba(0, 0, 0, 0.15);
    }
    /* Handle on hover */
    ::-webkit-scrollbar-thumb:hover {
      background: rgba(0, 0, 0, 0.4);
    }
  }

  .footer {
    border-top: 1px solid ${({ theme }) => theme.colors.grey[8]};
    padding: ${({ theme }) => theme.space(1)};
    .total {
      &__title {
        font-size: 0.7rem;
        color: ${({ theme }) => theme.colors.grey[4]};
        font-weight: 350;
      }
      &__price {
        margin-top: ${({ theme }) => theme.space(0.5)};
        > div {
          font-size: 1rem;
          font-weight: 400;
          color: ${({ theme }) => theme.colors.grey[2]};
        }
        > span {
          font-size: 0.7rem;
          font-weight: 400;
          color: ${({ theme }) => theme.colors.grey[4]};
          margin-top: ${({ theme }) => theme.space(0.5)};
          ${({ theme }) => theme.marginInlineStart(0.5)};
        }
      }
    }
    .checkout {
      margin: 0;
      min-height: 44px;
      border-radius: ${({ theme }) => theme.borderRadius.normal};
    }
  }
`;

export default StyledCartDetail;
