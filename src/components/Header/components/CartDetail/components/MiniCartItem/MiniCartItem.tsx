import { FC } from 'react';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import styled from 'styled-components';
import { Flex, Icon } from 'components';
import { CartItemType } from 'types/cart';
import { removeItemAction } from 'redux/actions/cart';

type Props = {
  className?: string;
  onClick?: () => void;
} & CartItemType;

const MiniCartItem: FC<Props> = ({ className, product, quantity, onClick }) => {
  const dispatch = useDispatch();

  const onRemoveCartItem = () => {
    dispatch(removeItemAction(product.id));
  };

  const moreLink = `/product/${product.id}/${encodeURIComponent(product.title)}`;

  return (
    <Flex className={className}>
      <Link to={moreLink} onClick={onClick}>
        <a>
          <img src={product.images.main} alt={product.title} />
        </a>
      </Link>
      <div className="info">
        <Link to={moreLink} onClick={onClick}>
          <a>
            <div className="info__title">{product.title}</div>
          </a>
        </Link>
        <Flex jc="space-between" fullWidth>
          <div className="info__qunatity">{quantity} عدد</div>
          <div className="info__trash" onClick={onRemoveCartItem}>
            <Icon icon={faTrash} />
          </div>
        </Flex>
      </div>
    </Flex>
  );
};

const StyledMiniCartItem = styled(MiniCartItem)`
  padding: ${({ theme }) => theme.space(1)};
  border-bottom: 1px solid ${({ theme }) => theme.colors.grey[9]};
  img {
    width: 70px;
    height: 70px;
    object-fit: contain;
    object-position: center;
    margin: ${({ theme }) => theme.space(1)};
  }
  .info {
    width: 100%;
    padding: ${({ theme }) => theme.space(1)};
    &__title {
      font-size: 0.7rem;
      color: ${({ theme }) => theme.colors.grey[2]};
      font-weight: 500;
      margin-bottom: ${({ theme }) => theme.space(1)};
    }
    &__qunatity {
      font-size: 0.8rem;
      color: ${({ theme }) => theme.colors.grey[2]};
      margin-top: ${({ theme }) => theme.space(1)};
      font-weight: 400;
    }
    &__trash {
      cursor: pointer;
      font-size: 0.9rem;
      padding: ${({ theme }) => theme.space(1)};
      ${({ theme }) => theme.marginInlineStart('auto')};
      ${({ theme }) => theme.marginInlineEnd(-1)};
      color: ${({ theme }) => theme.colors.grey[6]};
    }
  }
`;

export default StyledMiniCartItem;
