import { FC, ReactElement } from 'react';
import styled from 'styled-components';
import { faShoppingBasket } from '@fortawesome/free-solid-svg-icons';

import { Icon, PopOver } from 'components';

interface Props {
  className?: string;
  cartDetail?: ReactElement;
  count?: number;
}

const QuickCart: FC<Props> = ({ className, cartDetail }) => {
  return (
    <div className={className}>
      <PopOver position="bottom-left" mode="click" content={cartDetail}>
        <Icon icon={faShoppingBasket} className="basket-button" />
      </PopOver>
    </div>
  );
};

const StyledQuickCart = styled(QuickCart)`
  .basket-button {
    cursor: pointer;
    font-size: 1.3rem;
    position: relative;

    padding: ${({ theme }) => `${theme.space(1)} ${theme.space(2)}`};
    ${({ theme }) => theme.marginInlineEnd(-1)};
    border-radius: ${({ theme }) => theme.borderRadius.normal};
    color: ${({ theme }) => theme.colors.grey[3]};
    &::after {
      display: ${({ count }: Props) => (!count ? 'none' : 'initial')};
      content: '${({ count }: Props) => count ?? 0}';
      position: absolute;
      right: ${({ theme }) => theme.space(0.4)};
      bottom: ${({ theme }) => theme.space(0.4)};

      min-width: 20px;
      height: 20px;
      padding-top: 1px;

      font-size: 0.9rem;
      text-align: center;
      color: white;
      font-weight: 400;

      border-radius: 6px;
      background: ${({ theme }) => theme.colors.primary.main};
      border: 2px solid ${({ theme }) => theme.colors.grey[9]};
    }
  }
`;

export default StyledQuickCart;
