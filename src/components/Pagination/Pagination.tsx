import {
  faAngleDoubleLeft,
  faAngleDoubleRight,
  faChevronLeft,
  faChevronRight
} from '@fortawesome/free-solid-svg-icons';
import { FC } from 'react';
import styled from 'styled-components';
import { PageItem } from './components';

interface Props {
  className?: string;
  pageCount: number;
  activePage?: number;
  onPageChange?: (page: number) => void;
  range?: number;
}

const Pagination: FC<Props> = ({
  className,
  pageCount,
  onPageChange,
  activePage = 1,
  range = 7
}) => {
  if (pageCount <= 0) {
    return <div />;
  }
  const createElements = () => {
    const elements = [];

    const sidesCount = (range - 1) / 2;
    const from = activePage - sidesCount;
    const to = activePage + sidesCount;

    for (let k = 1; k <= pageCount; k++) {
      if (k >= from && k <= to)
        elements.push(
          <PageItem
            goPage={onPageChange}
            key={k}
            index={k}
            active={activePage === k}
          />
        );
    }

    return elements;
  };

  return (
    <div className={className}>
      <PageItem
        icon={faAngleDoubleRight}
        index={1}
        disabled={activePage <= 1}
        goPage={onPageChange}
      />
      <PageItem
        icon={faChevronRight}
        index={activePage - 1}
        disabled={activePage <= 1}
        goPage={onPageChange}
      />
      <div className="spacer" />
      {createElements()}
      <div className="spacer" />
      <PageItem
        icon={faChevronLeft}
        index={activePage + 1}
        disabled={activePage === pageCount}
        goPage={onPageChange}
      />
      <PageItem
        icon={faAngleDoubleLeft}
        index={pageCount}
        disabled={activePage === pageCount}
        goPage={onPageChange}
      />
    </div>
  );
};

const StyledPagination = styled(Pagination)`
  .spacer {
    width: ${({ theme }) => theme.space(2)};
    display: inline-block;
  }
`;

export default StyledPagination;
