import clsx from 'clsx';
import { FC } from 'react';
import styled from 'styled-components';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { Icon } from 'components';

interface Props {
  className?: string;
  index?: number;
  active?: boolean;
  icon?: IconProp;
  disabled?: boolean;
  goPage?: (index: number) => void;
}

const PageItem: FC<Props> = ({
  className,
  index,
  icon,
  active = false,
  goPage,
  disabled
}) => {
  return (
    <div
      onClick={() => (index && !disabled ? goPage?.(index) : null)}
      className={clsx(
        className,
        active && '--active',
        'noselect',
        disabled && '--disabled'
      )}>
      {icon ? <Icon icon={icon} /> : null}
      {!icon && index ? index : null}
    </div>
  );
};

const StyledPageItem = styled(PageItem)`
  display: inline-block;
  cursor: pointer;
  background: white;
  border: 1px solid ${({ theme }) => theme.colors.grey[8]};
  border-radius: ${({ theme }) => theme.borderRadius.normal};
  height: 40px;
  min-width: 40px;
  margin-left: ${({ theme }) => theme.space(0.5)};
  margin-right: ${({ theme }) => theme.space(0.5)};
  transition: ease border-color 0.2s;
  text-align: center;
  padding-top: ${({ theme }) => theme.space(1.2)};

  font-size: 0.9rem;
  color: ${({ theme }) => theme.colors.grey[3]};

  &:hover {
    border-color: ${({ theme }) => theme.colors.grey[6]};
  }

  &.--active {
    background: ${({ theme }) => theme.colors.primary.main};
    color: white;
    &:hover {
      border-color: ${({ theme }) => theme.colors.primary.light};
    }
  }
  &.--disabled {
    opacity: 0.4;
    cursor: initial;
    &:hover {
      border-color: ${({ theme }) => theme.colors.grey[8]};
    }
  }
`;

export default StyledPageItem;
