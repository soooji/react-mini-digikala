import { FC } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { faFrown } from '@fortawesome/free-solid-svg-icons';
import { Flex, Icon } from 'components';
import clsx from 'clsx';

interface Props {
  className?: string;
  title?: string;
}

const ErrorView: FC<Props> = ({
  className,
  title = 'خطایی در دریافت اطلاعات رخ داده!'
}) => {
  return (
    <Flex
      fd="column"
      ai="center"
      jc="center"
      className={clsx(className, 'common-card')}>
      <Icon icon={faFrown} />
      <div>{title}</div>
      <Link to="/">
        <a>بازگشت به صفحه اصلی</a>
      </Link>
    </Flex>
  );
};

const StyledErrorView = styled(ErrorView)`
  color: ${({ theme }) => theme.colors.grey[5]};
  min-height: 400px;
  margin-bottom: ${({ theme }) => theme.space(2)};
  width: 100%;
  span {
    font-size: 2rem;
  }
  div {
    margin-top: ${({ theme }) => theme.space(0.5)};
    font-size: 0.9rem;
    font-weight: 400;
  }
  a {
    margin-top: ${({ theme }) => theme.space(1)};
    padding-bottom: 2px;
    font-size: 0.8rem;
    color: ${({ theme }) => theme.colors.primary.main};
    border-bottom: dotted 1px ${({ theme }) => theme.colors.primary.main};
  }
`;

export default StyledErrorView;
