import { Flex } from 'components';
import { FC } from 'react';
import styled from 'styled-components';

type BreadItemType = {
  title: string;
  link: string;
};

type Props = {
  className?: string;
  items: BreadItemType[];
};

const BreadCrumb: FC<Props> = ({ className, items = [] }) => {
  return (
    <div className={className}>
      <Flex fw="wrap">
        {items.map((v, k) => (
          <div key={k}>
            {k !== 0 ? <span>/</span> : null}
            <a href={v.link}>{v.title}</a>
          </div>
        ))}
      </Flex>
    </div>
  );
};

const StyleBreadCrumb = styled(BreadCrumb)`
  span {
    font-size: 0.6rem;
    color: ${({ theme }) => theme.colors.grey[5]};
    margin-left: ${({ theme }) => theme.space(1)};
    margin-right: ${({ theme }) => theme.space(1)};
  }
  a {
    font-weight: 400;
    font-size: 0.75rem;
    color: ${({ theme }) => theme.colors.grey[4]};
    &:hover {
      color: ${({ theme }) => theme.colors.grey[2]};
    }
  }
`;

export default StyleBreadCrumb;
