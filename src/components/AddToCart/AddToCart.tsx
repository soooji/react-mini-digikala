import { FC } from 'react';
import styled from 'styled-components';
import clsx from 'clsx';
import {
  faMinus,
  faPlus,
  faShoppingBasket
} from '@fortawesome/free-solid-svg-icons';
import { useDispatch } from 'react-redux';

import { Icon, Flex } from 'components';
import { updateCartAction } from 'redux/actions/cart';
import { ProductType } from 'types/product';

interface Props {
  className?: string;
  product: ProductType;
  quantity: number;
  type?: 'small' | 'large';
  secondary?: boolean;
}

const AddToCart: FC<Props> = ({
  className,
  product,
  quantity,
  type = 'small',
  secondary = false
}) => {
  const dispatch = useDispatch();

  const onUpdateQuantity = (isRemove: boolean) => {
    dispatch(updateCartAction(product, isRemove ?? false));
  };

  return (
    <Flex
      className={clsx(
        className,
        quantity === 0 && 'new',
        'noselect',
        secondary && '--secondary'
      )}
      onClick={() => (quantity === 0 ? onUpdateQuantity(false) : null)}
      jc="space-between"
      ai="center"
      ac="center">
      {quantity === 0 ? (
        <>
          <Icon className="new__icon" icon={faShoppingBasket} />
          {type === 'large' ? (
            <span className="new__txt">افزودن به سبد خرید</span>
          ) : null}
        </>
      ) : (
        <>
          <Flex
            className="btn __remove"
            jc="center"
            ai="center"
            onClick={() => onUpdateQuantity(true)}>
            <Icon icon={faMinus} />
          </Flex>
          <div className="count">{quantity}</div>
          <Flex
            className="btn"
            jc="center"
            ai="center"
            onClick={() => onUpdateQuantity(false)}>
            <Icon icon={faPlus} />
          </Flex>
        </>
      )}
    </Flex>
  );
};

const StyledAddToCart = styled(AddToCart)`
  border-radius: 9px;
  width: fit-content;

  background-color: ${({ theme }) => theme.colors.primary.main};
  padding: ${({ theme }) => theme.space(0.5)};
  ${({ theme }) => theme.marginInlineEnd('auto')};

  &.new {
    justify-content: center;
    cursor: pointer;
    transition: ease background-color 0.15s, ease transform 0.15s;
    transform: scale(1);
    color: white;

    height: ${({ type }: Props) => (type === 'large' ? '50px' : '44px')};
    width: ${({ type }: Props) => (type === 'large' ? '300px' : '44px')};

    .new__icon {
      font-size: 1.1rem;

      margin-top: ${({ theme }) => theme.space(0.5)};
    }

    .new__txt {
      font-size: 0.9rem;
      font-weight: 400;

      ${({ theme }) => theme.marginInlineStart(1)};
    }

    &:hover {
      background-color: ${({ theme }) => theme.colors.primary.dark};
    }
    &:active {
      transform: scale(0.98);
    }
  }

  .count {
    font-size: 1rem;
    font-weight: 500;
    text-align: center;
    color: white;
    min-width: ${({ type }: Props) => (type === 'large' ? '100px' : '36px')};
  }

  .btn {
    background-color: white;
    cursor: pointer;
    font-size: 1rem;
    padding-top: 4px;
    border-radius: 6px;
    transition: ease color 0.15s, ease transform 0.15s;
    transform: scale(1);

    width: ${({ type }: Props) => (type === 'large' ? '40px' : '36px')};
    height: ${({ type }: Props) => (type === 'large' ? '40px' : '36px')};
    color: ${({ theme }) => theme.colors.primary.main};

    &.__remove {
      background: rgba(255, 255, 255, 0.9);
    }

    &:hover {
      color: ${({ theme }) => theme.colors.primary.dark};
    }
    &:active {
      transform: scale(0.95);
    }
  }

  &.--secondary {
    background-color: white;
    border: 1px solid ${({ theme }) => theme.colors.grey[9]};

    .btn {
      background-color: ${({ theme }) => theme.colors.primary.lightest};
      color: ${({ theme }) => theme.colors.primary.main};
      &.__remove {
        opacity: 0.8;
      }

      &:hover {
        color: ${({ theme }) => theme.colors.primary.dark};
      }
    }
    .count {
      color: ${({ theme }) => theme.colors.grey[3]};
    }
  }
`;

export default StyledAddToCart;
