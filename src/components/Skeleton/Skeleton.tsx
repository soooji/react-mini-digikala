import { FC } from 'react';
import styled from 'styled-components';
import * as CSS from 'csstype';

interface Props {
  className?: string;
  width?: CSS.Property.Width;
  height?: CSS.Property.Width;
  borderRadius?: CSS.Property.BorderRadius;
}

const Skeleton: FC<Props> = ({ className }) => {
  return <div className={className} />;
};

const StyledSkeleton = styled(Skeleton)`
  ${(props: Props) => (props.width ? `width: ${props.width}` : '')};
  ${(props: Props) => (props.height ? `height: ${props.height}` : '')};
  max-width: 100%;

  border-radius: ${(props: Props) => props.borderRadius ?? '8px'};

  background-color: white;
  background-image: linear-gradient(
    0.25turn,
    transparent,
    ${({ theme }) => theme.colors.grey[9]},
    transparent
  );
  background-repeat: no-repeat;
  background-position: -315px 0, 0 0, 0px 190px, 50px 195px;
  animation: loading 1.5s infinite;

  @keyframes loading {
    to {
      background-position: 315px 0, 0 0, 0 190px, 50px 195px;
    }
  }
`;

export default StyledSkeleton;
