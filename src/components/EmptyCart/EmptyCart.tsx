import { FC } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { faFrown } from '@fortawesome/free-solid-svg-icons';
import { Flex, Icon } from 'components';

interface Props {
  className?: string;
}

const EmptyCart: FC<Props> = ({ className }) => {
  return (
    <Flex fd="column" ai="center" jc="center" className={className}>
      <Icon icon={faFrown} />
      <div>سبد خرید شما خالی است!</div>
      <Link to="/search">
        <a>مشاهده همه محصولات</a>
      </Link>
    </Flex>
  );
};

const StyledEmptyCart = styled(EmptyCart)`
  color: ${({ theme }) => theme.colors.grey[5]};
  height: 100%;
  span {
    font-size: 2rem;
  }
  div {
    margin-top: ${({ theme }) => theme.space(0.5)};
    font-size: 0.9rem;
    font-weight: 400;
  }
  a {
    margin-top: ${({ theme }) => theme.space(1)};
    padding-bottom: 2px;
    font-size: 0.8rem;
    color: ${({ theme }) => theme.colors.primary.main};
    border-bottom: dotted 1px ${({ theme }) => theme.colors.primary.main};
  }
`;

export default StyledEmptyCart;
