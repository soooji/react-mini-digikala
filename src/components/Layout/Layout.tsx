import { FC, ReactNode, ReactElement } from 'react';
import styled from 'styled-components';

interface Props {
  children?: ReactNode;
  className?: string;
  header?: ReactElement;
}

const LayoutCMP: FC<Props> = ({ children, className, header }) => {
  return (
    <div className={className}>
      {header}
      <div className="page-container">{children}</div>
    </div>
  );
};

const Layout = styled(LayoutCMP)`
  padding: ${({ theme }) => theme.space(1)};
  > .page-container {
    margin-top: calc(65px);
  }
`;

export default Layout;
