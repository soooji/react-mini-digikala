import clsx from 'clsx';
import { useState, ReactElement } from 'react';
import styled from 'styled-components';
import { useOutsideClickRef } from 'rooks';

type PopOverStatusType = 'hover' | 'click';

interface Props {
  children: ReactElement;
  content?: ReactElement;
  title?: string;
  className?: string;
  fullWidth?: boolean;
  minWidth?: string;
  mode?: PopOverStatusType;
  position?: 'bottom-right' | 'bottom-left';
}

const PopOverCMP = ({
  children,
  className,
  mode = 'hover',
  title,
  content
}: Props): ReactElement => {
  const [open, setOpen] = useState<boolean>(false);

  const onClickOutside = () => {
    if (mode === 'click') setOpen(false);
  };

  const getEvent = (): Record<string, unknown> => {
    switch (mode) {
      case 'click':
        return {
          onClick: () => setOpen(!open)
        };

      default:
        return {
          onMouseEnter: () => setOpen(true),
          onMouseLeave: () => setOpen(false)
        };
    }
  };

  const [wrapperRef] = useOutsideClickRef(onClickOutside);
  const cn = className;

  return (
    <div ref={wrapperRef} className={cn}>
      <div {...getEvent()}>{children}</div>
      <div className={clsx(`${cn}__container`, open && '__open')}>
        <div className={`${cn}__container__menu`}>
          {title ? (
            <div className={`${cn}__container__menu__title`}>
              <p>{title}</p>
            </div>
          ) : null}
          <div className={`${cn}__container__menu__content`}>{content}</div>
        </div>
      </div>
    </div>
  );
};

const PopOver = styled(PopOverCMP)`
  width: ${({ fullWidth }) => (fullWidth ? '100%' : 'auto')};
  position: relative;
  display: inline-block;
  &__container {
    z-index: 1111;
    position: absolute;
    top: 100%;
    padding-top: ${({ theme }) => theme.space(1.5)};
    opacity: 0;
    transform: translateY(${({ theme }) => theme.space(1)});
    transition: ease opacity 0.1s, ease transform 0.2s;
    transition-delay: 0s, 0s;
    visibility: hidden;
    left: ${({ position }: Props) =>
      position === 'bottom-left' ? '0' : 'initial'};
    right: ${({ position }: Props) =>
      position === 'bottom-right' ? '0' : 'initial'};
    &.__open {
      transform: translateY(0);
      visibility: visible;
      opacity: 1;
    }
    &__menu {
      min-width: 150px;
      min-height: 250px;
      border-radius: ${({ theme }) => theme.borderRadius.normal};
      background-color: white;
      box-shadow: ${({ theme }) => theme.shadows[4]};
      &__title {
        p {
          padding: ${({ theme }) => theme.space(1.5)};
          text-align: center;
          margin: 0;
          font-size: 0.9rem;
          color: ${({ theme }) => theme.colors.grey[5]};
          width: 100%;
          border-bottom: 1px solid ${({ theme }) => theme.colors.grey[6]};
        }
      }
      &__content {
      }
    }
  }
`;

export default PopOver;
