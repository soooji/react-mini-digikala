import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`

  body {
     background-color: ${({ theme }) => theme.colors.grey[9]};
     margin: 0;
     direction: rtl;
  }
  a {
    text-decoration: none;
    color: initial;
  }
  * {
    box-sizing: border-box;
    font-family: 'iranyekanFaNum',Tahoma, sans-serif;
    /* width */
    ::-webkit-scrollbar {
      width: 7px;
      height: 7px;
      border-radius: 20px;
    }
    /* Track */
    ::-webkit-scrollbar-track {
      border-radius: 20px;
      background: rgba(0,0,0,.1);
    }
    /* Handle */
    ::-webkit-scrollbar-thumb {
      border-radius: 20px;
      background: rgba(0,0,0,.15);
    }
    /* Handle on hover */
    ::-webkit-scrollbar-thumb:hover {
      background: rgba(0,0,0,.4);
    }
  }
  .noselect {
    -webkit-touch-callout: none; /* iOS Safari */
      -webkit-user-select: none; /* Safari */
      -khtml-user-select: none; /* Konqueror HTML */
        -moz-user-select: none; /* Old versions of Firefox */
          -ms-user-select: none; /* Internet Explorer/Edge */
              user-select: none; /* Non-prefixed version, currently
                                    supported by Chrome, Edge, Opera and Firefox */
    }

  .common-card {
    background: white;
    box-shadow: ${({ theme }) => theme.shadows[0]};
    border-radius: ${({ theme }) => theme.borderRadius.outside};
  }
`;

export default GlobalStyle;
