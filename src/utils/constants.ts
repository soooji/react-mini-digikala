export const BASE_API = process.env.REACT_APP_BASE_API;
export const API_TOKEN = process.env.REACT_APP_API_TOKEN;

export const apiConfig = {
  headers: { token: API_TOKEN }
};

export const CURRENCY = 'تومان';
