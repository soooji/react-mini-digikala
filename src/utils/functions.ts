import qs from 'qs';
import { CartItemType, TotalPricesType } from 'types/cart';

export function getLS(key: string): string {
  const val = localStorage.getItem(key) || '';
  return ['undefined', 'null', 'NaN'].indexOf(val) > -1 ? '' : val;
}

export const makeId = (length: number): string => {
  let result = '';
  const characters =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
};

export function addCommas(num: number): string {
  const nStr = `${num}`;
  const x = nStr.split('.');
  let x1 = x[0];
  const x2 = x.length > 1 ? '.' + x[1] : '';
  const rgx = /(\d+)(\d{3})/;
  while (rgx.test(x1)) {
    x1 = x1.replace(rgx, '$1' + ',' + '$2');
  }
  return x1 + x2;
}

export function getLocationQuery<T>(search: string): T | Record<string, unknown> {
  if (!search || search.length === 0) return {};

  try {
    return qs.parse(search.substr(1));
  } catch {
    return {};
  }
}

export const scrollToTop = (): void => {
  try {
    window.scrollTo({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  } catch {
    //
  }
};

// Computes
export const computeDiscountPercent = (
  price: number,
  sellingPrice: number
): number => {
  if (!sellingPrice || !price) return 0;
  const discountAmount = price - sellingPrice;
  return Math.round((discountAmount / price) * 100);
};

export const computeCartTotals = (cart: CartItemType[]): TotalPricesType => {
  const totals: TotalPricesType = {
    discount: { price: 0, percent: 0 },
    realPrice: 0,
    payable: 0
  };

  cart.forEach((cartItem) => {
    totals.realPrice += cartItem.product.price.rrp_price * cartItem.quantity;
    totals.payable += cartItem.product.price.selling_price * cartItem.quantity;
  });

  totals.discount.price = totals.realPrice - totals.payable;
  totals.discount.percent = Math.round(
    (totals.discount.price / totals.payable) * 100
  );

  return totals;
};

export const getProductQuantity = (
  cart: CartItemType[],
  productId: number
): number => {
  const found = cart.find((c) => c.product.id === productId);
  return found?.quantity ?? 0;
};

export const getTotalQuantity = (cart: CartItemType[]): number => {
  let totalQ = 0;
  cart.forEach((c) => (totalQ += c.quantity));
  return totalQ;
};

export const getPageTitle = (title?: string): string => {
  return !title ? 'فروشگاه اینترنتی دیجی‌کالا' : `دیجی‌کالا :: ${title}`;
};

export const priceConvertor = (price: number): number => {
  if (price < 10) return 0;
  return Math.ceil(price / 10);
};
