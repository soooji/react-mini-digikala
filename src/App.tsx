import { FC, Suspense, StrictMode } from 'react';
import styled, { ThemeProvider } from 'styled-components';
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from 'redux/store';

import 'assets/fonts/iranyekan/css/fontiran.css';
import 'assets/fonts/iranyekanFaNum/css/fontiran.css';
import 'assets/css/normalize/_normalize.scss';
import GlobalStyle from 'GlobalStyle';

import history from 'utils/history';
import theme from 'utils/theme';
import { Layout, Header } from 'components';
import { Search, Product, Cart } from 'views';

interface Props {
  className?: string;
}

const App: FC<Props> = ({ className }) => {
  return (
    <StrictMode>
      <Provider store={store}>
        <ThemeProvider theme={theme}>
          <GlobalStyle />
          <div className={className}>
            <Router history={history}>
              <Suspense fallback={<p>در حال بارگزاری</p>}>
                <Layout header={<Header />}>
                  <Switch>
                    <Route path="/" exact>
                      <Redirect to="/search" />
                    </Route>
                    <Route path="/search" exact>
                      <Search />
                    </Route>
                    <Route path="/product/:slug">
                      <Product />
                    </Route>
                    <Route path="/cart" exact>
                      <Cart />
                    </Route>
                  </Switch>
                </Layout>
              </Suspense>
            </Router>
          </div>
        </ThemeProvider>
      </Provider>
    </StrictMode>
  );
};

const StyledApp = styled(App)``;

export default StyledApp;
