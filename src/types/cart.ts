import { ProductType } from './product';

export type CartItemType = {
  product: ProductType;
  quantity: number;
};

export type TotalPricesType = {
  discount: {
    price: number;
    percent: number;
  };
  realPrice: number;
  payable: number;
};
