export type ProductType = {
  id: number;
  title: string;
  rating: {
    rate: number;
    count: number;
  };
  status: 'marketable';
  images: {
    main?: string;
  };
  price: {
    selling_price: number;
    rrp_price: number;
  };
};
